from django.db import models

# Create your models here.
class Question(models.Model):
    question_text = models.CharField(max_length=255)

    def __str__(self):
        return '{} - {}'.format(self.id, self.question_text)
        
