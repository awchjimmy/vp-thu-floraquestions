import random
from django.shortcuts import render

from .models import Question

# Create your views here.
def home(request):
    rndpk = random.randint(1, Question.objects.count())
    q = Question.objects.get(pk=rndpk)
    return render(request, 'question/home.html', {'q': q})
    
